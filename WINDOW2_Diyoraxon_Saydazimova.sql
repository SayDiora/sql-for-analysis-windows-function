WITH SubcategorySales AS (
    SELECT
        prod_subcategory_id,
        EXTRACT(YEAR FROM time_id) AS order_year,
        SUM(amount_sold) AS yearly_sales,
        LAG(SUM(amount_sold)) OVER (PARTITION BY prod_subcategory_id ORDER BY EXTRACT(YEAR FROM time_id)) AS prev_year_sales
    FROM
        sh.sales
        JOIN sh.products ON sales.prod_id = products.prod_id
    WHERE
        EXTRACT(YEAR FROM time_id) BETWEEN 1998 AND 2001
    GROUP BY
        prod_subcategory_id, EXTRACT(YEAR FROM time_id)
)

SELECT DISTINCT
    prod_subcategory_id
FROM
    SubcategorySales
WHERE
    yearly_sales > COALESCE(prev_year_sales, 0);
